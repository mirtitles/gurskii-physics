% !TEX root = Gurskii-Elementary-Physics.tex
%\thispagestyle{empty}
\clearpage

\cleardoublepage

\chapter*{Introduction}
\label{ch-00}
\markboth{}{Introduction}
\section*{SI System of Units}
\label{sec-i1}

The analysis of physical phenomena and processes is associated with measuring physical quantities. To measure a physical quantity is to compare it with a similar quantity which is conventionally adopted as a unit.

Such a unit could be chosen for every quantity independently of other quantities. It is expedient, however, to proceed in a different way, i.e. to establish the units of several quantities (which are called \textbf{base units}) independently and then express the remaining units in terms of the base units by using physical regularities. For example, velocity is expressed in terms of two independent quantities, viz. length and time. The units established not independently but with the help of formulas relating them to base units are called \textbf{derived units}. The set of base and derived units is called a \textbf{system of units}.

In 1960, the \emph{XI General Conference on Weights and Measures} in Paris adopted the International System of Units (SI) which was subsequently supplemented and refined. It includes seven base units: metre (\si{\meter}) as the unit of length, kilogram (\si{\kilo\gram}) as the unit of mass, second (\si{\second}) as the unit of time, ampere (\si{\ampere}) as the unit of electric current, kelvin (\si{\kelvin}) as the unit of absolute (thermodynamic) temperature, candela (\si{\candela}) as the unit of luminous intensity, and mole (\si{\mole}) as the unit of the amount of substance. Supplementary SI units are radian (\si{\radian}) as the unit of plane angle and steradian (\si{steradian}) as the unit of solid angle.

Using mathematical expressions for physical quantities in terms of other quantities, all the remaining units of physical quantities can be expressed in terms of the base units. For example, the SI unit of force is a derived unit: a newton (\si{\newton}) is the force that imparts an acceleration of \SI{1}{\meter\per\second\squared} to a body whose mass is \SI{1}{\kilo\gram}.

Special standards are \textbf{manufactured} to keep the base units unchanged. These are measures and measuring instruments intended for the storage and reproduction of units of physical quantities to the highest accuracy that can be attained at a given level of science and engineering. The selection and definition of units, as well as the storage and reproduction of their standards, form a special branch of science called \emph{metrology}.

\textsf{\hlred{Definitions and standards of the SI base units}}
\begin{enumerate}[label=\arabic*.]
\item Prior to the \emph{XI General Conference on Weights and Measures}, the international standard metre was a Platinum-Iridium bar kept in Paris. The distance between the marks engraved on this bar at \SI{0}{\celsius} was taken as a metre. This distance was close to one ten-millionth of a quarter of the Paris meridian measured at the end of the 18th century. The accuracy of this standard was limited by the width of the marks, which is insufficient at the present time. Besides, such an artificial standard would be difficult to reproduce if it were lost or damaged. For this reason, the \emph{XI General Conference on Weights and Measures} held in 1960 established that a \textbf{metre} will be defined as the distance equal to \num{1650763.73} wavelengths of the radiation corresponding to the translation between orange-red energy levels of the Crypton-86 atom in vacuum. This conference specified the construction of the source of these waves, viz. a crypton-discharge lamp.

\item The SI unit of mass, viz. a \textbf{kilogram}, is equal to the mass of the international prototype of kilogram. The standard kilogram is a platinum-iridium prototype adopted in 1901 at the \emph{III General Conference on Weights and Measures} and kept in Paris.

\item In order to define the unit of time, viz. a second, more accurately, the \emph{XIII General Conference on Weights and Measures} associated it with the period of the radiation corresponding to the transition between certain levels of the Caesium-133 atom (earlier, it was associated with the motion of the Earth around its axis or with the motion of the Earth around the Sun). A \textbf{second} is equal to \num{9192631770} periods of the radiation corresponding to the transition between the two hyperfine levels of the ground state of the caesium-133 atom. The standard of the unit of time is reproduced with the help of a special caesium atomic oscillator.
\item The definition of the unit of temperature, viz. a \textbf{kelvin}, is given in \sect{sec-3.8}. Gas thermometers are used as standard instruments for reproducing the unit of temperature.

\item The previously used definition of the SI unit of current in terms of the mass of the substance deposited on an electrode in electrolysis could not ensure the sufficient accuracy of the standard. For this reason, the present-day SI unit of current is established on the basis of the law of interaction of thin parallel rectilinear current-carrying conductors of infinite length (see \sect{sec-4.26}). In spite of the fact that it is impossible to manufacture thin conductors of infinite length, the force of interaction of thin conductors of finite length can be calculated to a sufficiently high degree of accuracy by the law describing the interaction of conductors of infinite length. The instrument for reproducing the standard unit of current, viz. an \textbf{ampere}, is based on this law and is called the current (or ampere) balance.
\item The SI unit of luminous intensity, viz. a \textbf{candela}, is equal to the luminous intensity of light emitted in a given direction by a source of monochromatic radiation of frequency \SI{540d12}{\hertz}. The radiant intensity of this source in this direction is 1/683\si{\watt\per\steradian}. Incandescent lamps of various designs and various colour temperatures serve as the standard candela.
\item The unit of the amount of substance, viz. a \textbf{mole}, is the amount of substance in a system which contains as many structural elements as there are atoms in the Carbon-12 sample whose mass is \SI{0.012}{\kilo\gram}. When a mole is used, structural elements must be specified. They can be atoms, molecules, ions, electrons, and other particles or structural groups of particles.
\end{enumerate}

\section*{Vectors. Some Mathematical Operations \\on Vectors}
\label{sec-i2}
A vector is defined by its magnitude and direction (\figr{fig-001}). 

\begin{marginfigure}%[!ht]
\centering
\includegraphics[width=0.8\textwidth]{figures/fig-001.pdf}
\caption{A vector has a magnitude and direction.}
\label{fig-001}
\end{marginfigure}
Depending on the point of application, vectors can be of different types, i.e. bound, sliding, and free vectors.

A vector whose point of application is defined in space and cannot be displaced is called a \textbf{bound vector} (\figr{fig-001}~\drkgry{(a)}). A vector whose point of application can be displaced along the line of its action\sidenote{\footnotesize The line of action is a straight line containing the vector. The vector has one of the two possible directions, along this line of action or against it.} is called a \textbf{sliding vector} (\figr{fig-001}~\drkgry{(b)}). Finally, a vector whose point of application can be displaced to any point in space is called a free vector (\figr{fig-001}~\drkgry{(c)}).

Vectors are denoted by bold-face letters $(\vb{v}, \,\vb{a})$ or by an arrow above the letters ( $\vec{AB}$, where $A$ and $B$ are the tail and head of the vector). The magnitude of a vector is denoted as $\abs{\vb{a}}$, $\abs{\vec{AB}}$ or by the same letters as the vector itself but in light face $(v, \,a,\, AB)$.

\begin{marginfigure}
\centering
\includegraphics[width=0.6\textwidth]{figures/fig-002.pdf}
\caption{Sum of two vectors can be found out by drawing a parallelogram.}
\label{fig-002}
\end{marginfigure}
The \textbf{sum of two} vectors applied at the same point of a body is represented (in magnitude and direction) by the diagonal of the parallelogram plotted with the component vectors as its sides (\figr{fig-002}). Instead of constructing a parallelogram, we can plot a triangle (\figr{fig-003}). From the head of one vector, say, $\vb{a}$, we draw a vector $\vb{b'}$ which is equal in magnitude and parallel to the vector $\vb{b}$. Connecting the tail of vector a with the head of vector $\vb{b'}$, we obtain a vector equal to their sum. Comparing this figure with \figr{fig-002}, we see that instead of a parallelogram, we have plotted one of the triangles constituting it. This method of addition of vectors is called the \textbf{triangle rule}.

\begin{marginfigure}
\centering
\includegraphics[width=0.6\textwidth]{figures/fig-003.pdf}
\caption{Sum of two vectors can be found out by drawing a triangle.}
\label{fig-003}
\end{marginfigure}

The \textbf{addition of several vectors} lying in the same plane and applied at the same point at an angle to each other can be carried out consecutively. First the vector sum of two vectors is found, then this vector is added to a third vector, and so on. Alternately, the vectors can be added pairwise, the resultant vectors are also added in pairs, and so on.
\begin{marginfigure}%[!ht]
\centering
\includegraphics[width=0.8\textwidth]{figures/fig-004.pdf}
\caption{Adding multiple vectors using the polygon rule.}
\label{fig-004}
\end{marginfigure}

\figr{fig-004}~\drkgry{(a)} shows a consecutive addition of four vectors with the help of the triangle rule. The addition of vectors $\vb{F_{1}}$ and $\vb{F_{2}}$ gives their vector sum $\vb{R_{1}}$. Adding this vector to vector F3by the triangle rule, we obtain their sum $\vb{R_{2}}$. Finally, adding $\vb{R_{2}}$ and $\vb{F_{4}}$, we get the vector sum $\vb{R}$ of all the given four vectors.

Instead of all these intermediate constructions, we can proceed in a simpler way shown in \figr{fig-004}~\drkgry{(b)}. From the head $A$ of vector $\vb{F_{1}}$, we draw vector $\vec{AB}$ equal in magnitude and parallel to vector $\vb{F_{2}}$. From the head of this vector, we plot vector $\vec{BC}$ equal in magnitude and parallel to vector $\vb{F_{3}}$, and proceed in the same way until we have used all the given vectors. We thus obtain an unclosed polygon $OABCD$. Vector $\vec{OD}$ closing this polygon and opposite to the direction of circumvention (see \figr{fig-004}~\drkgry{(b)}) of the unclosed polygon constructed from the given vectors represents the sum of these vectors. This rule of addition of several vectors is called the \textbf{polygon rule}.

This method can be used for the addition of several free vectors as well as sliding vectors whose \emph{lines of action intersect at one point}. Since sliding vectors can be displaced along the lines of their action to the point of intersection of these lines, their resultant is applied at the same point or lies on a straight line passing through this point and coinciding with the resultant. \figr{fig-005}~\drkgry{(a)} illustrates the addition of three vectors $\vb{F_{1}}$, $\vb{F_{2}}$, and $\vb{F_{3}}$, whose lines of action intersect at point 0. 

\begin{marginfigure}%[!ht]
\centering
\includegraphics[width=\textwidth]{figures/fig-005.pdf}
\caption{Adding multiple vectors using the polygon rule.}
\label{fig-005}
\end{marginfigure}

The vector polygon is constructed separately in  \figr{fig-005}~\drkgry{(b)}. The vector sum is applied at point 0 or at any other point lying on the straight line $AOB$, say, at point $C$.

Free vectors whose lines of action do not intersect at one point can be added pairwise (according to the parallelogram or triangle rule).
In the particular case of the addition of vectors directed along a straight line, the polygon degenerates into a straight line on which the tail of the second vector coincides with the head of the first, the tail of the third vector with the head of the second, and so on. \figr{fig-005}~\drkgry{(c)} shows the addition of three vectors $\vb{F_{1}}$, $\vb{F_{2}}$, and $\vb{F_{3}}$ applied at point 0. One of the components, $\vb{F_{3}}$, and the resultant $\vb{R}$ are somewhat displaced for the sake of clarity.

It follows from what has been said above that \emph{vectors are added geometrically} unlike scalar quantities which are added algebraically.

The \emph{subtraction of one vector from another} is the same as the addition of the ``minuend'' vector and a vector equal in magnitude and antiparallel to the ``subtrahend'' vector.

The addition and subtraction of vectors directed along the same straight line can be simplified by replacing the geometrical addition and subtraction by the algebraic addition of projections of these vectors onto an axis having the same direction. In this case, as usual, the projections of vectors having the same direction as the chosen axis are assumed to be positive and the projections of vectors having the opposite directions are assumed to be negative. For example, vectors $\vb{F_{1}}$ and $\vb{F_{2}}$ in \figr{fig-005}~\drkgry{(c)} have the same direction as the $X$-axis, and their projections are positive. On the other hand, the projection of vector $\vb{F_{3}}$ onto this axis is negative. Instead of the vector equality
\begin{equation*}%
\vb{R} = \vb{F_{1}} + \vb{F_{2}} + \vb{F_{3}}
\end{equation*}
we can write
\begin{equation*}%
R = F_{1} + F_{2} - F_{3}.
\end{equation*}
If $\vb{F_{3}} > \vb{F_{1}}+ \vb{F_{2}}$, the sum of the three vectors is negative, i.e. vector $\vb{R}$ has the same direction as vector $\vb{F_{3}}$ (see \figr{fig-005}~\drkgry{c}).

When a vector $\vb{F}$ is multiplied by a scalar quantity $\alpha$, we obtain a vector $\alpha\vb{F}$ applied at the same point. Its magnitude is $\abs{\alpha}$ times larger than the magnitude $\vb{F}$ of the vector being multiplied. The direction of the resultant vector coincides with that of original vector if $\alpha > 0$ and is opposite to it if $\alpha < 0$.

\textbf{Multiplication of two vectors.} There are two kinds of vector products: the dot (scalar) product and the cross (vector) product. 

As a result of \textbf{scalar multiplication} of two vectors (which is denoted by $\vb{F_{1}}\vb{F_{2}}$ or $\vb{F_{1}} \vdot \vb{F_{2}}$), we obtain a scalar quantity equal to the product of the magnitudes of these vectors by the cosine of the angle formed by these vectors:
\begin{equation*}%
\vb{F_{1}}\vb{F_{2}}= F_{1}F_{2}\cos (\widehat{\vb{F_{1}},\, \vb{F_{2}}}).
\end{equation*}
As a result of vector multiplication of two vectors (which is denoted by $[\vb{F_{1}}\vb{F_{2}}]$ or $\vb{F_{1}}\cross \vb{F_{2}}$), we obtain a \textbf{vector} whose magnitude is equal to the area of the parallelogram constructed on these two vectors:
\begin{equation*}%
\abs{\vb{F_{1}} \cross \vb{F_{2}}} = F_{1}F_{2}\sin (\widehat{\vb{F_{1}},\, \vb{F_{2}}}).
\end{equation*}
\begin{marginfigure}%[1cm]
\centering
\includegraphics[width=0.5\textwidth]{figures/fig-006.pdf}
\caption{Cross product of vectors is not commutative.}
\label{fig-006}
\end{marginfigure}
The line of action of this vector is normal to the plane in which the two vectors being multiplied lie. Its direction is such that as we look from its head, the shortest rotation about this vector from the first to the second vector being multiplied is in the anticlockwise direction (\figr{fig-006}).\sidenote[][-5.5cm]{\footnotesize In practice, the ``corkscrew rule'' is conveniently used: the direction of the resultant vector in a vector product coincides with the direction of translation of a corkscrew whose handle is rotated from the first vector to the second along the shortest path.} Hence it follows that vector product is \emph{not commutative}, i.e. $\vb{F_{1}} \cross \vb{F_{2}}\neq \vb{F_{2}} \cross \vb{F_{1}}$. Instead, we have $\vb{F_{1}} \cross \vb{F_{2}}= - \vb{F_{2}} \cross \vb{F_{1}}$. In other words, a change in the order of vectors in a vector (cross) product leads to a vector having the same magnitude but the opposite direction.


The \textbf{decomposition of a vector into components} in two directions is carried out in accordance with the parallelogram rule. The vector being decomposed is the diagonal and the component vectors are the sides of the parallelogram. In the particular case of vector decomposition in two \emph{mutually perpendicular directions}, the parallelogram becomes a rectangle. \figr{fig-007} illustrates the decomposition of vector $\vb{F}$ into vectors $\vb{F_{1}}$ and $\vb{F_{2}}$ along the coordinate axes. The magnitudes of the component vectors are $\vb{F_{1}} = F \cos \alpha$ and $\vb{F_{2}}= F \sin \alpha$.
\begin{marginfigure}[-3cm]
\centering
\includegraphics[width=0.7\textwidth]{figures/fig-007.pdf}
\caption{A vector decomposed along two mutually perpendicular directions.}
\label{fig-007}
\end{marginfigure}

\section*{Projections of Points and Vectors onto an Axis}
\label{sec-i3}
The \textbf{projection of a point} $A$ onto a straight line (axis) $OO'$ is the base a of the perpendicular dropped from this point onto the straight line (\figr{fig-008}).
\begin{marginfigure}
\centering
\includegraphics[width=0.6\textwidth]{figures/fig-008.pdf}
\caption{Projection of a point onto a straight line.}
\label{fig-008}
\end{marginfigure}
The \textbf{projection of a vector} $\vec{AB}$ (and also $\vec{A'B'}$ , $\vec{CD}$, and $\vec{C'D'}$) onto the axis $OO'$ is the segment ab (and also $a'b'$, $cd$, and $c'd'$) bounded by the projections of the tail and head of the vector onto this axis (\figr{fig-009}). If the direction from the projection of the vector tail to the projection of its head coincides with the direction chosen for the axis, the projection of the vector is assumed to be positive $(ab, a'b')$. Otherwise, the projection is negative $(cd,c'd')$.
\begin{figure}[!ht]
\centering
\includegraphics[width=0.8\textwidth]{figures/fig-009.pdf}
\caption{Adding multiple vectors using the polygon rule.}
\label{fig-009}
\end{figure}

It can be seen from \figr{fig-009} that the projection of a vector onto an axis is equal to the product of the vector length and the cosine of the angle between the positive direction of the axis and the vector:
\begin{alignat*}{2}%
ab &= AB \cos \alpha_{1}, \quad  a'b' &&= A'B'\cos \alpha_{2}\\
cd &= CD \cos \alpha_{3},  \quad c'd'  &&= C'D'\cos \alpha_{4}
\end{alignat*}
As is usually done in trigonometry, angles are measured in the anticlockwise direction. An angle in the clockwise direction is assumed to be negative. (In actual practice, the cosine of the acute angle between the axis and vector is taken, and the sign of the projection is determined from the drawing.)

It follows from what has been said above that the projection of a vector onto an axis is a scalar quantity. \emph{All mathematical operations on projections are carried out algebraically.}


\section*{General Methodical Hints to the Solution \\of Problems}
\label{sec-i4}
The main peculiarity of a physical problem is that a \emph{physical} process is considered in it. Although the solution of the problem is reduced to a number of mathematical operations, the correct solution of the problem in physics is possible only if the physical process involved is understood correctly. Therefore, the following hints for solving physical problems are appropriate here.
\begin{enumerate}[label=\arabic*.]
\item The formulation of the problem should be read very attentively and more than once until it becomes clear which physical process or phenomenon is actually considered in it.
\item After this, the given and required physical quantities should be written down. The quantities that are given should be written thoroughly, without missing anything. The values of the quantities which are not given explicitly but which can be immediately obtained from the conditions of the problem should also be written. For example, if a braking process leads to a complete halt, we must write that the final velocity $v_{\text{f}} = 0$. If the conditions of a problem imply that some quantity $x$ can be neglected, it is necessary to write that $x \simeq 0$ or $x = 0$, and so on.
\item Next, the physical laws governing the given process should be recollected, as well as the mathematical formulas describing these laws. If there are several formulas describing these laws, we must compare the quantities appearing in the formulas with those given in the problem and choose the formulas containing the given and required quantities.
\item As a rule, a problem in physics is solved in the general form, i.e. we derive the formula in which the required quantity is expressed in terms of the given quantities. Finally, the numerical values of the given quantities are substituted together with their dimensions into the obtained formula. Thus, the numerical value and dimensions of the required quantity are determined. Solving the problem in this way, we do not accumulate errors as in the case when the approximate values of intermediate quantities are calculated and are then substituted into the formula for obtaining the value of the required quantity. The exceptions to this rule (which are quite rare) are of two types: \begin{enumerate*}[label=(\alph*)]\item the formula for calculating an intermediate quantity is so cumbersome that the calculation of this quantity simplifies the subsequent form of the solution; \item the numerical solution of the problem is much simpler than the derivation of the final formula and does not deteriorate the accuracy of the result.\end{enumerate*}

\item Before substituting the numerical values of given quantities into the computational formulas obtained for required quantities, all the values of the given quantities should be recalculated to express them in the same system of units, preferably in SI. An exception to this rule is the case when formulas contain a certain quantity as a multiplier in the numerator and denominator. Such quantities can be expressed in any (naturally, the same) units.
\item The correctness of the obtained result can be judged to a certain extent from dimensional analysis. If the computational formula is a sum of several terms, the dimensions of the addends should be the same.
\item The correctness of the result can also be controlled by the order of magnitude of the obtained numerical values. The order must be commensurate with the physical meaning of the required quantity.
\item The answer should be obtained to a certain degree of accuracy corresponding to the accuracy of given quantities. However, insufficient and excessive accuracy are equally harmful. For example, if the initial quantities are measured or given to within \SI{1}{\centi\meter}, and as a result of calculations we obtain \SI{287}{\milli\meter}, it is expedient to write the answer either in the form of \SI{29}{\centi\meter}, or \SI{0.29}{\meter} rather than \SI{28.7}{\centi\meter}, or \SI{0.287}{\meter}. On the other hand, if the initial quantities are given to within \SI{1}{\milli\meter}, and we get \SI{29}{\centi\meter}, the answer should be written in the form of \SI{29.0}{\centi\meter}, or \SI{290}{\milli\meter}, or \SI{0.290}{\meter} rather than \SI{0.29}{\meter}, or \SI{29}{\centi\meter}.
\end{enumerate}
In this book, wherever possible, the problems following each topic are arranged in the increasing order of difficulty. More complicated problems are marked by asterisks ($\ast$).

